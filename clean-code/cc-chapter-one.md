# Clean Code Lektürtagebuch

> Diese Notizen sollen dazu dienen persönliche Highlights 
> aus dem Clean Code Buch niederschreibe und
> schriftlich abrufen kann.

## Vorwort

- Ga-Jol zählt zu den beliebtesten Süßigkeiten in Dänemar :>

- 5S Philosophie -> Clean Code
	- Seiri (Organisation, sortieren)
	- Seiton (Ordentlichkeit, aufräumen)
	- Seiso (Sauberkeit, wienern)
	- Seiketsu (Standardisierung)
	- Shukutse (Disziplin, Selbstdiziplin)

## Einführung

- Was bedeutet Clean Code?
> [Click here](https://www.osnews.com/images/comics/wtfm.jpg)

- Inhalt des Buches
	- Einteilung in 3 Teile
		1. Beschreibung von Clean Code (Prinzipien, Patterns, Techniken)
		2. Fallstudien
			- jede Fallstudie ist ein Beispel für die Bereinigung von Code
		3. Heuristiken & Smells 
	- Buch = harte Arbeit ... vorallem der 2. Teil

## Sauberer Code

> Man liest das Buch, weil man ein Programmierer ist und weil man ein besserer Programmierer werden will.
> Tutorial: HOWTO transform BAD INTO GOOD Code

### 1.1 Code, Code and more Code

- Code verschwindet niemals
- Code ist Sprache, wie wir Anforderungen ausdrücken

### 1.2 Schlechter Code

> Beispiel
- Unternehmen hat ein Produkt, welcher schlechten Code beinhaltet hat
	- Man hat sich auf "später das Chaos aufräumen" verlassen
	- WICHTIG: Gesetz von Leblanc: *Später gleich niemals'

### 1.3 Lebenszyklus eines Chaos

- Chaotischer Code verlangsamt Produktivität
	- eine Änderung
		- verursacht mehrere Defekte

- Beispiel: Das große Redesign 

- Das grundlegende Problem:
	- Zeitdruck -> chaotischer Code
	- Profis -> Wissen -> guter Code -> braucht Zeit

> Ein Programmierer, der saubereren Code schreibt, ist ein Künstler, der
> einen leeren Bildschirm mit einer Reihe von Transformationen in ein elegantes
> codiertes System umwandelt.

- viele Definitionen von sauberen Code (Bjarne Stroustrup, Grady Booch, Dave Thomas, ...)

- Fazit: Wir sind Autoren und unser Code muss von Drittlesern daher lesbar sein.

## 1.6 Pfadpfinder-Regel

> Hinterlasse den Campingplatz sauberer, als du ihn gefunden hast.

## 1.8 Zusammenfassung

**Das Buch verspricht nicht, dass der Leser DER BESTE PROGRAMMIERER wird.**

> Es will eher mitteilen, welche Tipps, Tricks, Werkzeuge und Gedankenprozesse gute Programmierer aufzeigen.