# Composer + Autoloader
## Notes
(Composer requires PHP 5.3.2+)

- if you are using git you want to add /vendor/ to your .gitignore

# composer install
### composer.lock
Ich habe gelesen, wenn man "composer install" verwendet, sollte ein composer.lock und en composer.json erstellt werden?
-> Benji fragen
- As mentioned above, the composer.lock file prevents you from automatically getting the latest versions of your dependencies.

> composer update äquivalent zu
> composer delete + composer install

composer init

> Unterschied zwischen psr-4 Standard und Classmap 

> psr-4 ist ein Standard, welcher dir angibt wie du dein Projektordnerstruktur und Namespacing dazu strukturieren musst, damit autoload
> für dich vieles einfach laden kann

> Classmap nimmt die Klassen wie du sie im Ordner hast