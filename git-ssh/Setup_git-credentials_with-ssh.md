# Short Version

### First

`ssh-keygen -t rsa -b 4096 -C "youremail@example.com"`

> Enter
> Enter
> Enter

### Second

`eval "$(ssh-agent -s)"`

`ssh-add ~/.ssh/id_rsa`

`clip < ~/.ssh/id_rsa.pub`


> Go to [github.com / gitlab.com]  > Settings > SSH & GPG Keys > New
> Paste key & choose good name for key

> Use SSH on projects & benefit


# Setup your git with SSH

Wenn ihr wie ich keine Lust habt jedes Mal, wenn ihr auf github pusht
eure Login Daten einzugeben, habe ich hier eine Anleitung geschrieben, 
wie man am Sichersten seine git-credentials speichern kann.

Ich weiß auch nicht was jeder Befehl ins tiefste Detail macht, aber 
im Endeffekt wird mit deiner E-Mail in Kombination mit deiner Computer/Laptop-
MAC Adresse und einem neuen Passwort ein Hash erstellt, welchen du mit github 
verbindest.

## Setup

Öffnet die Git Bash in irgendeinem Ordner

![gitbash](img/gitbash.PNG "git bash")

> Rechtsklick 
> Git Bash Here 

öffnen.

### 1. Generate SSH Key with git-bash
Tippt diesen Befehl in die Gitbash ein:

`ssh-keygen -t rsa -b 4096 -C "youremail@example.com"`

Dann sollte kommen:

> Enter file in which to save the key (/c/Users/r.quidet/.ssh/id_rsa):

Hier gebt ihr kein file ein, sondern drückt einfach Enter (ich weiß nicht was passiert, wenn ihr einen Namen eingibt)

Danach erscheint:

> Enter passphrase (empty for no passphrase):

Hier könnt ihr optional ein Passwort eingeben. Es kommt darauf an,
ob ihr nachher bei Befehlen wie `git push` oder `git pull` ein Passwort
eingeben wollt oder nicht. 

Ihr könnt optional halt auch ein "einfacheres Passwort" hier wählen.
Ich z.B. benütze online Lastpass für mein Github Passwort und müsste bei 
jeden Push zum Browser gehen, mein Passwort kopieren (kann es nicht auswendig)
und einfügen. Genau aus dem Grund habe ich es eigentlich eingerichtet :>

Danach musst du nochmal dein Passwort eintippen und
wenn du alles richtig gemacht hast, sollten ein paar Zeilen und ein RSA Bild erscheinen.
###### Schließe NICHT die Bash, habs zwar nicht ausprobiert, ob es einen Unteschied macht, aber
###### ;>

## github.com

Nach diesem Krampf gehst du auf die github.com Seite.
Klickst rechts oben auf dein Bild und gehst auf Settings.

![gitbash](img/github-settings.png "Github Settings")

Links solltest du dann irgendwo SSH and GPG keys sehen, auf den klickst du dann auch.

Hier solltest du ganz groß SSH keys und GPG keys sehen.
Klickst dann rechts oben auf

> New SSH key

### now you need git-bash again
Hier tippst du 3 Befehle ein:

`eval "$(ssh-agent -s)"`
`ssh-add ~/.ssh/id_rsa`
`clip < ~/.ssh/id_rsa.pub`

Jetzt habt ihr den SSH Key im Zwischenspeicher und könnt ihn auf github.com
hineinkopieren. Am Besten ihr gebt ihm den Namen "WorkPC" oder auf welchen PC
ihr auch das gerade eingerichtet habt.

## HTTPS to SSH
Jetzt müsst ihr nur mehr euer git repo von HTTPS auf SSH umstellen, mit folgenden Befehlen:

Geht am Besten z.B. in den game-of-life Ordner/Projekt und versucht das Projekt zu pushen.
Habt ihr immer noch HTTPS sollte bei diesem Befehl

`git remote -v`

folgende Adresse erscheinen:

> https://github.com/laola1-benji/game-of-life.git

Geht auf Benji sein Projekt auf Github und wählt unter Projekt klonen SSH aus.

![gitbash](img/https.png "HTTPS")
![gitbash](img/ssh.png "SSH")

und kopiert die Adresse

> git@github.com:laola1-benji/game-of-life.git

Danach schreibt ihr noch 

`git remote set-url origin git@github.com:laola1-benji/game-of-life.git`

in die git bash und versucht dann mal zu pushen.

Wenn ihr am Anfang ein Passwort angegeben hat, dann müsst ihr das eintippen, falls nicht, nicht.

### Notizen

Es gibt über HTTPS die Möglichkeit

`git config credential.helper store`

# ABER

Das Passwort wird dann plain in einem .txt File gespeichert, was garnicht sicher ist.

Die andere Möglichkeit wäre dann noch:

`git config credential.helper cache`

Welche das Passwort eine Zeitlang speichert

git config --global credential.helper "cache --timeout=3600"

find ich aber auch nervig 

## Links zum Nachlesen/Schauen

https://www.youtube.com/watch?v=H5qNpRGB7Qw&t=943s

https://stackoverflow.com/questions/5343068/is-there-a-way-to-skip-password-typing-when-using-https-on-github




