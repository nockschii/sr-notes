# PHP & OOP

If a parent class has a constructor the children class doesn't call the constructor automatically
https://stackoverflow.com/questions/217618/construct-vs-sameasclassname-for-constructor-in-php

classes are defined public by default (I think class methods and class members (properties) too)
http://php.net/manual/en/language.oop5.visibility.php
