# some Shortcuts and Snippets - phpStorm

CTRL + ÷ or CTRL + /	...		toggle Comment (Windows 10 hat leider ein Problem mit /, daher die Alternative auf dem Numpad mit ÷)

Shift + Shift			...		search Everywhere

#### Shortcuts
https://blog.jetbrains.com/webstorm/2015/06/10-webstorm-shortcuts-you-need-to-know/

#### Live Templates
https://blog.jetbrains.com/webstorm/2018/01/using-and-creating-code-snippets/


Shift + F6				... 	rename

CTRL + Q 				...		return types

ALT + INS (EINFG)		...		generate Functions

CTRL + I 				...		auto implement functions

CTRL + ALT + S 			...		Settings

ctrl + 