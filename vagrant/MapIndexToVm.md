# How to map a index.php to homestead/laravel

> Legt in eurem workspace Ordner (wahrscheinlich C:/workspace) ein Projekt an mit einer index.php

## 1. Geht in den Homesteadordner (in C:Windows/Users)

## 2. Tragt die zu mappende URL ein, mit dem richtigen Verzeichnis.
![homestead.yml](img/homesteadyml.png "homestead.yml")

## 3. Danach öffnet ihr die Datei hosts im folgenden Verzeichnis `C:\Windows\System32\drivers\etc`
![hosts_dir](img/hosts_dir.png "hosts_dir")

## 4. Tragt die VM IP und den zu mappende URL ein
![hosts](img/hosts.png "hosts_dir")

#5. Ruft die gemappte URL ein und freut euch.