# Setup for Windows

Dieses Setup ist eine extension für PHP, um PHP Code ohne einen Webserver debuggen zu können.

## Install xDebug

1. You need to know your PHP Version

`php -v`

My version
> PHP 7.1.26 NTS (Not Thread Safe)

2. Download xDebug file to right php version

![xDebugVersion](img/xDebug-versions.png "xDebug versions")

Alternative: Copy + Paste `php -i` output into
https://xdebug.org/wizard.php

You should have .dll file

3. Move .dll file to

> ../php7.1/ext 


4. Add in the end of the file ../php7.1/php.ini following settings  

> zend_extension = C:\php\php7.1\ext\php_xdebug-2.5.5-7.1-vc14-nts-x86_64.dll
> xdebug.remote_enable = 1
> xdebug.remote_hosts="localhost"
> xdebug.remote_port=9000

php -v should now recognize xDebug

## Setup xDebug JIT (Just in Time) for phpStorm

https://www.jetbrains.com/help/phpstorm/debugging-in-the-jit-mode.html


## Links I used

https://xdebug.org/docs/install

http://www.dorusomcutean.com/install-xdebug-windows-phpstorm/#htix

https://www.jetbrains.com/help/phpstorm/debugging-in-the-jit-mode.html